﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlTimer : MonoBehaviour
{
    public Text tiempoText;
    public float tiempo = 0.0f;

    // Update is called once per frame
    void Update(){
    
        tiempo = tiempo + Time.deltaTime;
        tiempoText.text = tiempo.ToString("0.00");
    }
}
