﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMele : MonoBehaviour
{   
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        this.gameObject.GetComponent<EnemyController>().Follow(GameObject.FindWithTag("Player"), 2);
        if (this.GetComponent<EnemyController>().vidas == 0){
            Destroy(this.gameObject);
            GameObject.FindWithTag("Player").GetComponent<PlayerController>().score = GameObject.FindWithTag("Player").GetComponent<PlayerController>().score + 150;
        }
    }

    void OnCollisionEnter2D (Collision2D other){
        if (other.gameObject.tag == "Player"){
            GameObject.FindWithTag("Player").GetComponent<PlayerController>().life--;
            Destroy(this.GetComponent<Rigidbody2D>());
        }else if (other.gameObject.tag == "DisparoBueno"){
            this.GetComponent<EnemyController>().vidas--;
        }
    }
}
