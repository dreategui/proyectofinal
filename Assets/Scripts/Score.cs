﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Score : MonoBehaviour
{

    public int gameScore = GameObject.FindWithTag("Player").GetComponent<PlayerController>().score;
    public string ScoreString = "Score";

    public Text TextScore;

    public static GameObject GameController;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (TextScore != null) 
        {
            TextScore.text = ScoreString + GameObject.FindWithTag("Player").GetComponent<PlayerController>().score; ;
        }
    }
}
