﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotBuenoController : MonoBehaviour
{
    Vector3 targetPosition;
    Vector3 movDisparo;
    // Start is called before the first frame update
    void Start(){
        Physics2D.IgnoreCollision(this.GetComponent<BoxCollider2D>(), GameObject.Find("Player").GetComponent<PolygonCollider2D>());
        targetPosition = Input.mousePosition;
        targetPosition.z = 0;
        movDisparo = targetPosition - this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Mathf.Abs(movDisparo.x) > 0.2f || Mathf.Abs(movDisparo.y) > 0.2f){//comprobamos si hemos llegado al player
            //calculamos el vector de movimiento hacia el player
            movDisparo.Normalize();
            movDisparo *= 6;
            movDisparo = new Vector3(movDisparo.x, movDisparo.y, movDisparo.z);
            //movemos el enemigo hacia el player
            this.transform.Translate(movDisparo*Time.deltaTime, Space.World);
        }
    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag!="Player" && coll.gameObject.tag!="base"){
            Destroy(this.gameObject);
        }
        if (coll.gameObject.tag == "enemy"){
            coll.gameObject.GetComponent<EnemyController>().vidas--;
        }
    }
}
