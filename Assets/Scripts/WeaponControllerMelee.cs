﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponControllerMelee : MonoBehaviour
{
    private Vector3 mouse_pos;
    private Vector3 object_pos;
    private float angle;
 
    void Update()
    {
        RotaArma();
    }

    void RotaArma(){
        mouse_pos = Input.mousePosition;
        mouse_pos.z = -20;
        object_pos = Camera.main.WorldToScreenPoint(this.transform.position);
        mouse_pos.x = mouse_pos.x - object_pos.x;
        mouse_pos.y = mouse_pos.y - object_pos.y;
        angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg;
        this.transform.rotation = Quaternion.Euler(0, 0, angle-90);
    }
    
    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "enemy"){
            coll.gameObject.GetComponent<EnemyController>().vidas--;
            if(coll.gameObject.name == "Kamikaze(Clone)"){
                GameObject.FindWithTag("Player").GetComponent<PlayerController>().life = GameObject.FindWithTag("Player").GetComponent<PlayerController>().life + 2;
                GameObject.FindWithTag("Player").GetComponent<PlayerController>().score = GameObject.FindWithTag("Player").GetComponent<PlayerController>().score + 150;
            }else{
                GameObject.FindWithTag("Player").GetComponent<PlayerController>().life++;
            }
        }
        if (coll.gameObject.name == "Shot(Clone)"){
            GameObject.FindWithTag("Player").GetComponent<PlayerController>().life++;
        }
    }
}
