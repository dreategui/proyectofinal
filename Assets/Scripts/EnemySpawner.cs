﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private float spotX;
    private float spotY;
    public GameObject enemigo;
    public float InstantiationTimer = 0f;
    Vector3 positionPlayer;
    Vector3 positionSpawn;
    private float distance = 0f;
    string[] enemigos = {"Melee", "Distance", "Kamikaze"};
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {   
        int i = Random.Range(0, 3);
        enemigo = GameObject.Find(enemigos[i]);
        spotX = Random.Range(-25f, 25f);
        spotY = Random.Range(-25f, 25f);
        positionPlayer = GameObject.FindWithTag("Player").GetComponent<Transform>().position;
        positionSpawn = new Vector3(spotX, spotY, 0);
        distance = Vector3.Distance(positionSpawn, positionPlayer);
        if (distance < 4f){
            spotX = Random.Range(-25f, 25f);
            spotY = Random.Range(-25f, 25f);
        }
        CreatePrefab(enemigo, spotX, spotY);
        enemigo.GetComponent<SpriteRenderer>().sortingOrder = 1;
        enemigo.GetComponent<EnemyController>().maxS = 2f;
    }
    public void CreatePrefab(GameObject objeto, float x, float y){
        InstantiationTimer -= Time.deltaTime;
        if (InstantiationTimer <= 0){
            Instantiate(objeto, new Vector2(x,y), Quaternion.identity);
            InstantiationTimer = 3f;
        }
    }
}
