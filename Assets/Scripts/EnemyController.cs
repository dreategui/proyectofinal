﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    //private Rigidbody2D rb2d;
    //private Animator anim;
    public GameObject player;
    public float maxS = 2f;
    public int vidas;
    //private float velRot = 12.0f;
    // Start is called before the first frame update
    void Start()
    {
        //rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }
    public void Follow(GameObject target, float thisSpeed){
		Vector3 positionEnemy = this.transform.position;
        Vector3 positionTarget = target.transform.position;
        //distancia entre enemigo y player
        Vector3 followTarget = positionTarget - positionEnemy;
        //followTarget = new Vector3(followTarget.x, followTarget.y, 0); //ignoramos el eje Z
        if (Mathf.Abs(followTarget.x) > 0.2f || Mathf.Abs(followTarget.y) > 0.2f){//comprobamos si hemos llegado al player
            //calculamos el vector de movimiento hacia el player
            followTarget.Normalize();
            followTarget *= thisSpeed;
            followTarget = new Vector3(followTarget.x, followTarget.y, followTarget.z);

            //movemos el enemigo hacia el player
            this.transform.Translate(followTarget*Time.deltaTime, Space.World);
        }
    }
}
