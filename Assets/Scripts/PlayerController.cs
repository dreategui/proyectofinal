﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    //private Animator anim;
    private float moveX = 0f;
    private float moveY = 0f;
    
    private float ratonEsquiva = 0f;
    public int life = 3;
    public float maxS = 100f;
    public int energia = 30;
    public GameObject Arma;
    public Sprite sprArmaMelee;
    public Sprite sprArmaDistancia;
    public int score = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        //anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (life <= 0) {
			Invoke("finalDead", 0.4F);
	    }
        
        moveX = Input.GetAxis("Horizontal");
        moveY = Input.GetAxis("Vertical");
        rb2d.velocity = new Vector2(moveX*maxS*Time.fixedDeltaTime, moveY*maxS*Time.fixedDeltaTime);
        
        ratonEsquiva = Input.GetAxis("Fire2");
        if (ratonEsquiva == 1f){
            Esquivar(energia);
            energia--;
        }
    }
    void finalDead(){
        SceneManager.LoadScene("Menu_dead");
    }
    void Esquivar(int energia){
        if ((moveX>0.01f || moveX<-0.01f || moveY>0.01f || moveY<-0.01f) && (energia > 0)){
            rb2d.velocity = new Vector2(moveX*10*maxS*Time.fixedDeltaTime, moveY*10*maxS*Time.fixedDeltaTime);
        }
    }
}
