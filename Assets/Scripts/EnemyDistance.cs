﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDistance : MonoBehaviour
{   
    public float alerta = 20f;
    public GameObject shot;
    private float shootingTimer = 2f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 thisPosition = this.transform.position;
        Vector3 playerPosition = GameObject.FindWithTag("Player").transform.position;
        Vector3 distanceToPlayer = playerPosition - thisPosition;
        if (distanceToPlayer.magnitude >=13f){
            this.gameObject.GetComponent<EnemyController>().Follow(GameObject.FindWithTag("Player"), 2);
        }else{
            Destroy(this.GetComponent<Rigidbody2D>());
            shootingTimer -= Time.deltaTime;
            if (shootingTimer <= 0f){
                ShooterTime(playerPosition);
                shootingTimer = 2f;
            }
        }
        
        if (this.GetComponent<EnemyController>().vidas == 0){
            Destroy(this.gameObject);
            GameObject.FindWithTag("Player").GetComponent<PlayerController>().score = GameObject.FindWithTag("Player").GetComponent<PlayerController>().score + 100;
        }
    }

    void OnCollisionEnter2D (Collision2D other){
        if (other.gameObject.tag == "Player"){
            GameObject.FindWithTag("Player").GetComponent<PlayerController>().life--;
            Destroy(this.GetComponent<Rigidbody2D>());
        }else if (other.gameObject.tag == "DisparoBueno"){
            this.GetComponent<EnemyController>().vidas--;
        }
    }

    void ShooterTime(Vector3 targetPosition){
        Instantiate(shot, new Vector2(this.transform.position.x, this.transform.position.y), Quaternion.identity);
        
    }
}
